#----------------------------------------
# Scott VanderHorst
# Credit to Joe Livecchi for the cool logic to check services at the end
# I copied off his work to correct other stuff too
# Tidal Service Restart
# 7/23/2019
#----------------------------------------

#----------------------------------------
# Loop de loop
#----------------------------------------

Do {

    #----------------------------------------
    # Variable declaration section
    #----------------------------------------

    $FaultMon = "PHCTIDAL03"
    $Master = "PHCTIDAL01"
    $Backup = "PHCTIDAL02"
    $ClientMgr = "PHCTIDAL04"
    $restart = $false

    #----------------------------------------
    # Stop All Services, wait
    #----------------------------------------

    Write-Host "Stopping Windows services on Tidal servers."
    Invoke-Command -ComputerName $FaultMon -ScriptBlock {Stop-Service -Name "TIDALSAFaultMon"}
    Write-Host "$FaultMon Services stopping." -ForegroundColor Green

    Invoke-Command -ComputerName $Master -ScriptBlock {Stop-Service -Name "TIDALSAMaster"}
    Write-Host "$Master Services stopping." -ForegroundColor Green

    Invoke-Command -ComputerName $Backup -ScriptBlock {Stop-Service -Name "TIDALSAMaster"}
    Write-Host "$Backup Services stopping." -ForegroundColor Green

    Invoke-Command -ComputerName $ClientMgr -ScriptBlock {Stop-Service -Name "TIDALClientMgr"}
    Write-Host "$ClientMgr Services stopping." -ForegroundColor Green
    Write-Host "Waiting 30 seconds."

    Start-Sleep -s 30

    #----------------------------------------
    # Start All Services in order
    #----------------------------------------

    Write-Host "Starting Services on $FaultMon"
    Invoke-Command -ComputerName $FaultMon -ScriptBlock {Start-Service -Name "TIDALSAFaultMon"}
    Write-Host "Service is starting on $FaultMon, waiting 30 seconds." -ForegroundColor Green

    Start-Sleep -s 30

    Write-Host "Starting Services on $Master"
    Invoke-Command -ComputerName $Master -ScriptBlock {Start-Service -Name "TIDALSAMaster"}
    Write-Host "Service is starting on $Master, waiting 30 seconds." -ForegroundColor Green

    Start-Sleep -s 30

    Write-Host "Starting Services on $Backup"
    Invoke-Command -ComputerName $Backup -ScriptBlock {Start-Service -Name "TIDALSAMaster"}
    Write-Host "Service is starting on $Backup, waiting 30 seconds." -ForegroundColor Green

    Start-Sleep -s 30

    Write-Host "Starting Services on $ClientMgr"
    Invoke-Command -ComputerName $ClientMgr -ScriptBlock {Start-Service -Name "TIDALClientMgr"}
    Write-Host "Service is starting on $ClientMgr, waiting 15 seconds." -ForegroundColor Green

    Start-Sleep -s 15

    #----------------------------------------
    # Verify Services are Running
    #----------------------------------------

    $machineAndServices = @{
        "PHCTIDAL01" = "TIDALSAMaster"
        "PHCTIDAL02" = "TIDALSAMaster"
        "PHCTIDAL03" = "TIDALSAFaultMon"
        "PHCTIDAL04" = "TIDALClientMgr"
    }
    $restart = $false
    foreach ( $machine in $machineAndServices.Keys) {
        [bool]$result = Invoke-Command -ComputerName $machine -ScriptBlock {
            param(
                $serviceName
            )
            
            Write-Host "Checking Service: $serviceName" 
            $ClientRunning = get-service -Name $serviceName
            If ($ClientRunning.Status -eq "Running") {
                Write-Host "$([System.Environment]::MachineName) service IS running. No Restart Needed :)" -ForegroundColor Green
                Write-Output $false
            }
            else {
                Write-Output $true
                Write-Host "$([System.Environment]::MachineName) service is NOT running. Restarting script" -ForegroundColor  Yellow
            }
        } -ArgumentList $machineAndServices[$machine]
        $restart = $restart -or $result
    }
    Write-Host "Is restart needed: $restart"

    #----------------------------------------
    # End of loop logic
    #----------------------------------------

} While ($restart -eq $true)

Write-Host "Services Successfully Restarted" -ForegroundColor Green
Read-Host "Debug"